;#!/bin/ash
;
;# Recreate missing directories (ignored when re-creating initramfs) IGNORED
;/bin/mkdir /home
;/bin/mkdir /boot
;/bin/mkdir /tmp
;/bin/mkdir /proc
;/bin/mkdir /sys
;
;# Mount pseudo file systems DONE
;mount -n -a
;
;# Start asynchronous shell script DONE
;/etc/init.d/rcShlpr&
;
;# Start udev DONE
;#/sbin/udevd --daemon&
;#/sbin/udevstart&
;#echo "/sbin/udev" >> /proc/sys/kernel/hotplug
;/sbin/mdev -s&
;echo "/sbin/mdev" > /proc/sys/kernel/hotplug
;
;# Syslog in circular buffer ( -C ) or not DONE
;/sbin/syslogd -C
;
;# Klogd DONE
;/sbin/klogd -c 2
;
;# Kmap
;busybox loadkmap < /etc/azerty.kmap
;
;# Hostname DONE
;/bin/hostname assra
;
;# Loopback DONE
;/sbin/ifconfig lo up
;
;# Clear screen
;#clear

format ELF executable 3

entry start

segment readable writeable
	mount_cmd db '/bin/mount',0
	mount_option db '-na',0

	rcShlpr_cmd db '/etc/init.d/rcShlpr',0

	hostname db 'assra_asm'
	hostname_size = $ - hostname

	udev_cmd db '/sbin/mdev',0
	udev_size = $ - udev_cmd -1
	udev_option db '-s',0
	udev_filename db '/proc/sys/kernel/hotplug',0

	syslog_cmd db '/sbin/syslogd',0
	syslog_option db '-C',0

	klogd_cmd db '/sbin/klogd',0
	klogd_op1 db '-c',0
	klogd_op2 db '2',0

	ifconfig_cmd db '/sbin/ifconfig',0
	ifconfig_op1 db 'lo',0
	ifconfig_op2 db 'up',0

segment readable executable

start:

;mount pseudo file systems

	mov eax, 2
	xor ebx, ebx
	int 0x80

	or eax, eax
	jnz wait_mount

exec_mount:
	push dword 0
	mov edx, esp
	push mount_option
	mov ebx, mount_cmd
	push mount_cmd
	mov ecx, esp
	mov eax, 11
	int 0x80

wait_mount:
	mov ebx, eax
	xor ecx, ecx
	xor edx, edx
	mov eax, 7
	int 0x80

;Start asynchronous shell script

	mov eax, 2
	xor ebx, ebx
	int 0x80

	or eax, eax
	jnz save_shell

daemonize_shell:
; NOTE: WE HAVE TO MAKE A DAEMON HERE
; setsid
	mov eax, 66
	int 0x80

; fork again
	mov eax, 2
	xor ebx, ebx
	int 0x80
	or eax, eax
	jz exec_shell

;exit
	mov eax, 1
	xor ebx, ebx
	int 0x80

exec_shell:
; Execve
	push dword 0
	mov edx, esp
	push rcShlpr_cmd
	mov ebx, rcShlpr_cmd
	mov ecx, esp
	mov eax, 11
	int 0x80

;exit child in case syscall failed
	mov eax, 1
	mov ebx, 1
	int 0x80

save_shell:
;	push eax if script is too late, reparenting by init won't be a problem

;Udev
	mov eax, 2
	xor ebx, ebx
	int 0x80

	or eax, eax
	jnz echo_udev

daemonize_udev:
; setsid
	mov eax, 66
	int 0x80

; fork again
	mov eax, 2
	xor ebx, ebx
	int 0x80

	or eax, eax
	jz exec_udev

	mov eax, 1
	xor ebx, ebx
	int 0x80

exec_udev:
	push dword 0
	mov edx, esp

	push udev_option
	push udev_cmd
	mov ebx, udev_cmd
	mov ecx, esp
	mov eax, 11
	int 0x80

;exit child
	mov eax, 1
	mov ebx, 1
	int 0x80

echo_udev:
;open file
	mov eax, 5
	mov ebx, udev_filename
	mov ecx, 1
	xor edx, edx
	int 0x80

;write
	mov eax, 4
	mov ebx, 3
	mov ecx, udev_cmd
	mov edx, udev_size
	int 0x80

;close
	mov eax, 6
	;ebx = 3
	int 0x80

;Syslog
	mov eax, 2
	xor ebx, ebx
	int 0x80

	or eax, eax
	jnz wait_syslog

exec_syslog:
	push dword 0
	mov edx, esp
	push syslog_option
	mov ebx, syslog_cmd
	push syslog_cmd
	mov ecx, esp
	mov eax, 11
	int 0x80

;exit child
	mov eax, 1
	mov ebx, 1
	int 0x80

wait_syslog:
	mov ebx, eax
	xor ecx, ecx
	xor edx, edx
	mov eax, 7
	int 0x80
;Klogd
	mov eax, 2
	xor ebx, ebx
	int 0x80

	or eax, eax
	jnz wait_klogd

exec_klogd:
	push dword 0
	mov edx, esp
	push klogd_op2
	push klogd_op1
	push klogd_cmd
	mov ebx, klogd_cmd
	mov ecx, esp
	mov eax, 11
	int 0x80

wait_klogd:
	mov ebx, eax
	xor ecx, ecx
	xor edx, edx
	mov eax, 7
	int 0x80

;Kmap

;Hostname

	mov eax, 74
	mov ebx, hostname
	mov ecx, hostname_size
	int 0x80

;Loopback
	mov eax, 2
	xor ebx, ebx
	int 0x80

	or eax, eax
	jnz wait_ifconfig

exec_ifconfig:
	push dword 0
	mov edx, esp
	push ifconfig_op2
	push ifconfig_op1
	push ifconfig_cmd
	mov ebx, ifconfig_cmd
	mov ecx, esp
	mov eax, 11
	int 0x80

; exit child
	mov eax, 1
	mov ebx, 1
	int 0x80

wait_ifconfig:
	mov ebx, eax
	xor ecx, ecx
	xor edx, edx
	mov eax, 7
	int 0x80
;exit

	mov eax, 1
	xor ebx, ebx
	int 0x80
